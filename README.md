# oc-cmsapi #

CmsAPI is an OctoberCMS plugin that provides GET routes for various OctoberCMS CmsObjects

## ROUTES

### CONTENT

``` 
/_cms/api/v1/content?name=<FILENAME>

Result:

{
  content: string,
  parsed: string
}
```

GET for Content objects. If content is markdown, it will be parsed to HTML server-side.

### PARTIAL

``` 
/_cms/api/v1/partial?name=<FILENAME>

Result:

{
  content: string,
  parsed: string
}
```

GET for Partial objects. Partial will be parsed with default Twig::parse method.

Warning! October Twig functions like |theme are not available here!

### PAGE

``` 
/_cms/api/v1/page?name=<FILENAME>

Result:

{
  content: string,
  parsed: string
}
```

GET for Page objects. Page will be parsed by main October's CmsController `run` method, 
which will bring you complete parsed page markup.
