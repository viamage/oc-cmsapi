<?php
/**
 * Created by IntelliJ IDEA.
 * User: jin
 * Date: 3/28/19
 * Time: 11:11 AM
 */

namespace Viamage\CmsApi\Controllers;

use Cms\Classes\CmsController;
use Cms\Classes\Content;
use Cms\Classes\Page;
use Cms\Classes\Partial;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Viamage\CmsApi\Exceptions\ObjectNotFoundException;

/**
 * Class ApiController
 * @package Viamage\CmsApi\Controllers
 */
class ApiController
{
    /**
     * @param string $type
     * @param string $name
     * @return JsonResponse
     */
    public function get(string $type): JsonResponse
    {
        $name = \Input::get('name');
        if(!$name){
            return \Response::json(['error' => true, 'message' => 'Parameter "name" is missing.'], 400);
        }

        try {
            switch ($type) {
                case 'content':
                    return $this->getContent($name);
                case 'partial':
                    return $this->getPartial($name);
                case 'page':
                    return $this->getPage($name);
                default:
                    return \Response::json(['error' => true, 'message' => 'Unsupported content type'], 501);
            }
        } catch (ObjectNotFoundException $e) {
            return \Response::json(['error' => true, 'message' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());

            return \Response::json(['error' => true, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param string $name
     * @return JsonResponse
     * @throws ObjectNotFoundException
     */
    private function getContent(string $name): JsonResponse
    {
        $contentList = Content::all()->toArray();
        if (!array_key_exists($name, $contentList)) {
            throw new ObjectNotFoundException('Content Not Found');
        }
        $content = $contentList[$name];
        $result = [
            'content' => $content['content'],
        ];
        if (str_contains($name, '.md')) {
            $result['parsed'] = \Markdown::parse($content['content']);
        }

        return \Response::json($result, 200);
    }

    /**
     * @param string $name
     * @return JsonResponse
     * @throws ObjectNotFoundException
     */
    private function getPartial(string $name): JsonResponse
    {
        $partialsList = Partial::all()->toArray();
        if (!array_key_exists($name, $partialsList)) {
            throw new ObjectNotFoundException('Content Not Found');
        }
        $content = $partialsList[$name];

        $result = [
            'content' => $content['content'],
            'parsed' => \Twig::parse($content['content'])
        ];

        return \Response::json($result, 200);
    }

    /**
     * @param string $name
     * @return JsonResponse
     * @throws ObjectNotFoundException
     */
    private function getPage(string $name): JsonResponse
    {
        $pagesList = Page::all()->toArray();
        if (!array_key_exists($name, $pagesList)) {
            throw new ObjectNotFoundException('Content Not Found');
        }
        $content = $pagesList[$name];
        $url = $content['url'];
        /** @var CmsController $controller */
        $controller = \App::make(CmsController::class);
        /** @var Response $page */
        $page = $controller->run($url);
        $result = [
            'content' => $content['content'],
            'parsed' => $page->getContent(),

        ];

        return \Response::json($result, 200);
    }

}
