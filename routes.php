<?php

Route::group(
    ['prefix' => '_cms/api/v1', 'middleware' => ['web']],
    function () {
        Route::get(
            '/{type}',
            function ($type) {
                $controller = App::make(\Viamage\CmsApi\Controllers\ApiController::class);

                return $controller->get($type);
            }
        );
    }
);
